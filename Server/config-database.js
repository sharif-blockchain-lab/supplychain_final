

var dbUser		=	'scmuser' ;
var dbHost		=	'database.postgres.vm';
var dbName		=	'scmdb';
var dbPassword	=	'scmpass';
var dbPort		=	5432 ;


module.exports.dbUser = dbUser ;
module.exports.dbName = dbName ;
module.exports.dbPassword = dbPassword ;
module.exports.dbPort = dbPort ;
module.exports.dbHost = dbHost ;
